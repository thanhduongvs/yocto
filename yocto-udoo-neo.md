# Yocto udoo neo

![Build Status](https://img.shields.io/badge/thanhduongvs-yocto%20udoo%20neo-green.svg)

## Quick Start Guide  

1. clone repository:  
- :computer: *$* `git clone https://gitlab.com/embedded-yocto/udoo-neo-bsp.git -b morty` 
- :computer: *$* `cd udoo-neo-bsp`  
2. build image:
- :pushpin: setup environment:  
- :computer: *udoo-neo-bsp$* `MACHINE=udoovn DISTRO=fslc-framebuffer source setup-environment build`
- :pushpin: download package:  
- :computer: *udoo-neo-bsp/build$* `bitbake -c fetchall udoo-image-qt5`
- :pushpin: build:  
- :computer: *udoo-neo-bsp/build$* `bitbake -k udoo-image-qt5` hoặc `bitbake udoo-image-qt5` .
- :pushpin: Có nghĩa là bitbake sẽ tìm đến file udoo-image-qt5.bb để build image. file udoo-image-qt5.bb tương đương file main.c.  
- :pushpin: Đối số `-k` có ý nghĩa khi build có lỗi thì sẽ bỏ qua lỗi đó và tiếp tục build, nếu không có `-k` thì khi gặp lỗi sẽ dừng lại không build tiếp nữa.
3. Burning the image to the SD-Card:  
- :computer: *udoo-neo-bsp/build$* `umount /dev/mmcblk0`
- :pushpin: `umount /dev/<disk>`
- :computer: *udoo-neo-bsp/build$* `bzcat tmp/deploy/images/udoovn/udoo-image-qt5-udoovn.wic.bz2 | sudo dd of=/dev/mmcblk0 bs=32M`
- :pushpin: `sudo dd of=/dev/<disk> bs=32M`

## edit
1. edit wifi:  
`udoo-neo-bsp/sources/meta-udoo/recipes-connectivity/wifi/files/wpa_supplicant1.conf`
2. edit user:  
`udooer@udooer`
3. chỉnh thư mục download vào thư mục udoo-neo-bsp/download gõ lệnh, để chọn thư mục download:
`ln -s /home/download`  
`bitbake comman list`  
`bitbake -f -c cleansstate <name_recipe>`  
`bitbake -f -c configure <name_recipe>`  
`bitbake -f -c compile <name_recipe>`  
`bitbake -f -c install <name_recipe>`  

## Cross compile a Qt5 SDK to use in QtCreator
- :computer: *udoo-neo-bsp/build$* `bitbake udoo-image-qt5 -c populate_sdk`
- :pushpin: file khi build xong vào thư mục */build/tmp/deploy/sdk*, chạy file **fslc-framebuffer-glibc-x86_64-udoo-image-qt5-armv7at2hf-neon-toolchain-2.2.3.sh**
- :computer: *udoo-neo-bsp/build$* `./tmp/deploy/sdk/fslc-framebuffer-glibc-x86_64-udoo-image-qt5-armv7at2hf-neon-toolchain-2.2.3.sh`
- :pushpin: Enter target directory for SDK (default: /opt/fslc-framebuffer/2.2.3)